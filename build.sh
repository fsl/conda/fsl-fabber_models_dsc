#!/usr/bin/env bash

set -e

export FSLDIR=$PREFIX
export FSLDEVDIR=$PREFIX

. $FSLDIR/etc/fslconf/fsl-devel.sh

make insertcopyright

mkdir -p $PREFIX/src/
cp -r $(pwd) $PREFIX/src/$PKG_NAME

# Fabber uses an indirect mechanism for dynamically
# loading different forward model implementations,
# with the effect that the fabber_<model> executables
# do not directly depend on any symbols defined in
# the associated model library. This causes issues
# under macOS/clang, whereby use of the
# -dead_strip_dylibs linker flag causes the library
# to be removed from the list of libraries that the
# executable needs to link against.
if [[ ! -z ${LDFLAGS} ]]; then
  export LDFLAGS=${LDFLAGS//-Wl,-dead_strip_dylibs}
fi

make
make install
